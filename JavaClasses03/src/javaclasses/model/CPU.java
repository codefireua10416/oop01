/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaclasses.model;

/**
 *
 * @author human
 */
public class CPU {

    // Fiels
    private int cores;
    private double frequency;

    // Accessors
    // Getter
    public int getCores() {
        return cores;
    }
    
    public void setCores(int value) {
        cores = value;
    }
    
    public double getFrequency() {
        return frequency;
    }
    
    public void setFrequency(double value) {
        frequency = value;
    }
}
