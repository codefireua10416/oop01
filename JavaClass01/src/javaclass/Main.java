/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaclass;

/**
 *
 * @author human
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        // Create object type of "Type".
        // Type name = new Type();
        
        Car bmw = new Car();
        Car ferrary = new Car();
        
        bmw.color = "red";
        bmw.engine = 4.2;
        bmw.speed = 300;
        
        ferrary.color = "red";
        ferrary.speed = 350;
        ferrary.engine = 5.;
        
        System.out.println("BMW");
        System.out.println(bmw.toString());
        System.out.println(bmw);
        
        System.out.println(bmw.hashCode());
        System.out.println(Integer.toString(bmw.hashCode(), 2));
        System.out.println(Integer.toString(bmw.hashCode(), 16));
        
        System.out.println(bmw.color);
        System.out.println(bmw.engine);
        System.out.println(bmw.speed);
        
        System.out.println("Ferrary");
        System.out.println(ferrary);
        System.out.printf("%s %s %s\n", ferrary.color, ferrary.engine, ferrary.speed);
    }
    
}
