package ua.com.terminal;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author All
 */
public class Type {

    boolean cash;
    private Card card1;

    public Card getCard1() {
        return card1;
    }

    public void setCard1(Card card1) {
        this.card1 = card1;
    }

    public void setCash(boolean cash) {
        this.cash = cash;
    }

    @Override
    public String toString() {
        return "Type:\n    " + card1;
    }

}
