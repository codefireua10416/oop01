package ua.com.terminal;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author All
 */
public class Card {
    // Fiels

    private int number;
    private int pin;

    // Accessors
    // Getter
    public int getNumber() {
        return number;
    }

    public void setNumber(int n) {
        number = n;
    }

    public int getPin() {
        return pin;
    }

    public void setPin(int p) {
        pin = p;
    }

    @Override
    public String toString() {
        return "Card number: " + number + "\n    Pin: " + pin;
    }

}
