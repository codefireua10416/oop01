package ua.com.terminal;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author All
 */
public class Pay {

    private Type payType;

    public Type getPayType() {
        return payType;
    }

    public void setPayType(Type payType) {
        this.payType = payType;
    }

    @Override
    public String toString() {
        return "Pay " + payType;
    }
}
