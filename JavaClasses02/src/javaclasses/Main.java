/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaclasses;

/**
 *
 * @author human
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        CPU cpu = new CPU();
        cpu.cores = 4;
        cpu.frequency = 2.14;
        
        HardDisk hd = new HardDisk();
        hd.type = "ssd";
        hd.volume = 500;
        
        Memory mem = new Memory();
        mem.capacity = 4;
        mem.frequency = 1600;
        
        Computer comp = new Computer();
        comp.disk = hd;
        comp.memory = mem;
        comp.processor = cpu;
        
        
    }
    
}
